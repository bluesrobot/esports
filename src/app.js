angular.module('eSports', ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/messages');
        $stateProvider
            .state('teams', {
                url: "/teams",
                templateUrl: "partials/teams.html"
            })
            .state('messages', {
               url: "/messages",
               templateUrl: "partials/messages.html",
               controller: 'MessageCtrl'
            });
        
    })
    .controller('MainCtrl', function($scope){
        $scope.blah = 'blah';
    })
    .controller('MessageCtrl', function($scope){
        var dummyMessages = [
                {
                    from: "Marketplace",
                    subject: "Your order has shipped!"
                },
                {
                    from: "Local LAN Battles",
                    subject: "Tournament entry confirmed"
                },
                {
                    from: "Bank",
                    subject: "Loan status"
                }
            ];
            
        $scope.messages = dummyMessages;
    })